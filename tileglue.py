#! /bin/env python3
"""Script to download one large image from a tiled map provider"""
import sys
import math
import requests
import time
import argparse
import multiprocessing
from PIL import Image
from io import BytesIO
from concurrent.futures import ThreadPoolExecutor
from requests_futures.sessions import FuturesSession


#house in hofstätten
hofstaetten_pos = (49.2815889, 7.8582037)
hofstaetten_small_upper_left = (49.283134, 7.851336)
hofstaetten_small_lower_right = (49.274855, 7.867787)
hofstaetten_big_upper_left = (49.324646, 7.740110)
hofstaetten_big_lower_right = (49.218637, 7.960887)

#satelite
satelite_url = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
#satelite roads
roads_url = "https://services.arcgisonline.com/ArcGIS/rest/services/Reference/World_Transportation/MapServer/tile/{z}/{y}/{x}"
#
topo_url = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}"
#
streets_url = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}"

def coords(s):
    try:
        lat, lon = map(float, s.split(','))
        return lat, lon
    except:
        raise argparse.ArgumentTypeError("Coordinates must be lat,lon and floating numbers")

def size(s):
    try:
        x, y = map(int, s.split(','))
        return x, y
    except:
        raise argparse.ArgumentTypeError("Size must be x,y and integer numbers")

parser = argparse.ArgumentParser(description='Download one large image from a tile map provider.')
parser.add_argument('-z', '--zoom_level', default=18, type=int,
                    help='the zoom level of the map (default=18 is highest zoom level i.e. the most detailed)')
parser.add_argument('-o', '--output', default='karte.tiff', #type=argparse.FileType('w'),
                    help='output file (the ending will determine the filetype produced by this program')
parser.add_argument('--box', nargs=2, metavar=('upper_left', 'lower_right'),
                    default=(hofstaetten_small_upper_left, hofstaetten_small_lower_right), type=coords,
                    help='bounding box of the exported region in "lat,lon" for each coodinate')
parser.add_argument('--url', nargs='+', default=[satelite_url, roads_url],
                    help='List of URLs end points of the tile server from which the image is to fetch. \
Normally ending in "tile/{z}/{y}/{x}" where x,y,z are substitued by this programm to fetch the complete image.')
parser.add_argument('--image_size', metavar=('width,height'), default=(256,256), type=size,
                    help='size of one image at the end point')

def deg2num(lat_deg, lon_deg, zoom):
    """lat, lon, zoom to tiled x,y coordinates"""
    lat_rad = math.radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n)
    return (xtile, ytile)

def num2deg(xtile, ytile, zoom):
    """tiled x, y, zoom to lat, long coordinates"""
    n = 2.0 ** zoom
    lon_deg = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    lat_deg = math.degrees(lat_rad)
    return (lat_deg, lon_deg)

def download_image_row(y, upper_num, zoom, xtiles, session, url, results):
    try:
        time_start = time.time()
        s = FuturesSession(max_workers=5)
        futures = []
        for i in range(0, xtiles):
            current_url = url.format(x=upper_num[0] + i, y=upper_num[1] + y, z=zoom)
            futures.append(s.get(current_url))

        for i in range(0, xtiles):
            req = futures[i].result()
            img = Image.open(BytesIO(req.content))
            results[y].paste(img, (i * 256, 0))

        print("done row %d in %fs" % (y, time.time() - time_start))
    except Exception as e: 
        print(e)


def main():
    """Main entry point for the script."""
    args = parser.parse_args()
    upper_left = args.box[0]
    lower_right = args.box[1]

    time_start = time.time()

    url = args.url[0]
    session = requests.Session()

    upper_num = deg2num(upper_left[0], upper_left[1], args.zoom_level)
    lower_num = deg2num(lower_right[0], lower_right[1], args.zoom_level)
    xtiles = lower_num[0] - upper_num[0];
    ytiles = lower_num[1] - upper_num[1];
    print("size in tiles %dx%d" % (xtiles, ytiles))

    result_size = (xtiles * args.image_size[0], ytiles * args.image_size[1])
    print("size in pixels %dx%d" % result_size)
    result = Image.new('RGBA', result_size)
    results = []

    with ThreadPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
        for i in range(0, ytiles):
            results.append(Image.new('RGBA', (args.image_size[0] * xtiles, args.image_size[1])))
            executor.submit(download_image_row, i, upper_num, args.zoom_level, xtiles, session, url, results)

    for i in range(0, ytiles):
        img = results[i]
        result.paste(img, (0, i * args.image_size[1]))
    
    time_pre = time.time()
    result.save(args.output)
    print("saved result in %fs" % (time.time() - time_pre))
    print("running time %fs" % (time.time() - time_start))


if __name__ == '__main__':
    sys.exit(main())
